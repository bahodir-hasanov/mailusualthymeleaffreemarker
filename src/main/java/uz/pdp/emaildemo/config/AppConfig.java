package uz.pdp.emaildemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

// Bahodir Hasanov 4/4/2022 8:55 AM

@Configuration
public class AppConfig {
    @Primary
    @Bean
    public FreeMarkerConfigurationFactoryBean freeMarkerConfig() {
        FreeMarkerConfigurationFactoryBean config = new FreeMarkerConfigurationFactoryBean();
        config.setTemplateLoaderPath("classpath:/templates");
        return config;
    }
}
