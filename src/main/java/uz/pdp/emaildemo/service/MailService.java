package uz.pdp.emaildemo.service;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import uz.pdp.emaildemo.dto.MailDto;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// Bahodir Hasanov 4/4/2022 9:11 AM
@Service
public class MailService {
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    TemplateEngine thymeleafTemplateEngine;
    @Autowired
    FreeMarkerConfigurer freeMarkerConfigurer;


    public HttpEntity<?> sendMail(MailDto mailDto) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("hasanovbahodirq@gmail.com");
        message.setTo(mailDto.getTo());
        message.setSubject("Good morning");
        message.setText(mailDto.getMessage());
        javaMailSender.send(message);
        return ResponseEntity.ok("successfully sent message");
    }

    public HttpEntity sendMessageWithAttachment(MailDto mailDto) {
        MimeMessage message = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom("hasanovbahodirq@gmail.com");
            helper.setTo(mailDto.getTo());
            helper.setSubject(mailDto.getSubject());
            helper.setText(mailDto.getMessage());
            File file = ResourceUtils.getFile("classpath:static/perfect.jpg");
            helper.addAttachment("photo.xlsx", file);
            javaMailSender.send(message);
            return ResponseEntity.ok("successfully sent message!");
        } catch (MessagingException e) {
            e.printStackTrace();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("Ops sorry!");

    }



    public HttpEntity<?> sendMessageUsingThymeleafTemplate(MailDto mailDto) {
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("signature", "sign");
        templateModel.put("senderName", "Bob");
        templateModel.put("recipientName", "Jhon");
        templateModel.put("text", "Hello");
        templateModel.put("regards", "...");
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(templateModel);
        String htmlBody = thymeleafTemplateEngine.process("mailExp.html", thymeleafContext);
        HttpEntity<?> httpEntity = sendMassageWithTemplate(mailDto, htmlBody);
        return httpEntity;

    }
    public HttpEntity<?> sendMassageWithTemplate(MailDto mailDto, String htmlBody) {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(mailDto.getTo());
            helper.setSubject(mailDto.getSubject());
            helper.setText(htmlBody, true);
            javaMailSender.send(message);
            return ResponseEntity.ok("thymeleaf successfully sent message!");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HttpEntity<?> sendMassageUsingFreemarker(MailDto mailDto) {
        Map<String, Object> model = new HashMap<>();
        model.put("name", "Bob");
        model.put("message", "alhamdulillah");

        try {
            Template freemarkerTemplate = freeMarkerConfigurer.getConfiguration().getTemplate("welcome.ftl");
            String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, model);
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(mailDto.getTo());
            helper.setSubject(mailDto.getSubject());
            helper.setText(htmlBody, true);
            javaMailSender.send(message);

            return ResponseEntity.ok("Free marker template successfully sent message!");
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (TemplateNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (MalformedTemplateNameException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }
}
