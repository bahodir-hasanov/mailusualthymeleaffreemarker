package uz.pdp.emaildemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.emaildemo.dto.MailDto;
import uz.pdp.emaildemo.service.MailService;

// Bahodir Hasanov 4/4/2022 8:52 AM
@RestController
@RequestMapping("/api/email")
public class MailController {

    @Autowired
    MailService mailService;

    @PostMapping
    public HttpEntity<?> sendEmail(@RequestBody MailDto mailDto) {
        /**
         * 1. only message sending
         */

        //  return mailService.sendMail(mailDto);
        /**
         * 2. message with photo sending
         */
        //  return mailService.sendMessageWithAttachment(mailDto);

        /**
         * 3. template(thymeleaf) send email
         */
           // return mailService.sendMessageUsingThymeleafTemplate(mailDto);

        /**
         * 4. template(freemarker) send email
         */
          return mailService.sendMassageUsingFreemarker(mailDto);
    }
}
