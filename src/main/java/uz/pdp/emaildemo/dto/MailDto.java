package uz.pdp.emaildemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Bahodir Hasanov 4/4/2022 8:54 AM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MailDto {
    private String to;
    private String message;
    private String subject;

}
